/*

Definiciones de variables, funciones,
objetos y otras cosas que necesite...

*/

var i = 0;

function cambiarColor(el) {
     if (i == 0) {
	 el.style['color'] = '#CF8383';
	 i = 1;
     } else {
	 el.style['color'] = 'black';
	 i = 0;
     }
};


/*

Para tener acceso a todos los elementos del DOM
necesito estar seguro que la página terminó de
cargarse y renderizarse. 

Para eso utilizo el evento window.onload

*/

window.onload = function () {

    /*

    // // Si ocupo el selector getElementsByTagName voy a 
    // // modificar **todos** los párrafos del documento
    var parrafos = document.getElementsByTagName("p");
    console.log(parrafos);
    for(var i = 0; i < parrafos.length; i++) {
	parrafos[i].onclick = function () {
	    cambiarColor(this);
	}

	// // Esto no funciona, porque no se le asigna
	// // una **funcion** que maneja el evento
	//parrafos[i].onclick = cambiarColor(parrafos[i]);
	
    }*/

    // Para seleccionar específicamente un elemento
    // Necesito asignarle un atributo id único
    // y usar el selector document.getElementById
    var p1 = document.getElementById("parrafo1");
    p1.onclick = function () { cambiarColor(this); }
}
